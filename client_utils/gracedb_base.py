from ligo.gracedb.rest import GraceDb

def get_client(playground=False):
    """This function returns a connection to GraceDB.

    :param playground: if True the returned connection
                       will point to GraceDB-playground
    :return a connection to GraceDB
    """

    if not playground:
        return GraceDb()
    else:
        return GraceDb(service_url='https://gracedb-playground.ligo.org/api/')



def get_autocorr_and_latency(pg_client,
                             event_name,
                             autocorr_check=None,
                             ew_check=None,
                             verbose=False):
    """This function returns the autocorrelation length and latency variable
    of the bank responsible for the trigger.

    THE EXPECTATION IS THAT THE coinc.xml FILE HAS BEEN CREATED AND THAT:
    1. aucorrelation length is identified just before the early warning flag
       in the bank id submission flag.
       Here's an expectation of the format:
       "/home/manoj.kovalam/O3b/O3_Replay/
       banks/autocorr_1051/EW_50/iir_H1-GSTLAL_SPLIT_BANK_0006-a1-0-0.xml.gz"

    The current implementation pulls the coinc.xml file associated with the event,
    casts it a string. A 50 element subset is extracted by first finding the location
    of the following string element:
        "/autocorr_{autocorrelation length}/...".

    The autocorrelation length string is then found by finding the following "/" and removing "autocorr_".

    Following the extraction of the autocorrelation string, the remaining string should contain the
    latency string in the following format:
        "ew_{latency integer value}/.."

    The latency integer value is extracted by dropping "ew_" from the substring preceding the first "/"
    character.

    Example usage:

        from cit_utils.gracedb_base import get_GraceDB_client
        from cit_utils.gracedb_base import get_autocorr_and_latency

        pg_client = get_GraceDB_client(playground=True)
        event_name = 'M299629'

        autocorr, latency = get_autocorr_and_latency(pg_client, event_name)

        print 'autocorrelation length: {}\nlatency: {}'.format(autocorr, latency)


    :param pg_client: client connection to GraceDb
    :param event_name: string event name
    :param autocorr_check: string identifier of the autocorrelation parameter in the banks path
    :param ew_check: string identifier of the latency parameter in the banks path

    :return: autocorr_string, ew_string
    """
    response = pg_client.files(event_name, 'coinc.xml')
    file_contents = response.read()
    if autocorr_check is None:
        autocorr_check = "/autocorr_"

    if ew_check is None:
        ew_check = "ew_"

    autocorr_index = file_contents.find(autocorr_check)
    if autocorr_index < 0:
        if verbose:
            print(file_contents)

    if verbose:
        print('autocorr_index: {}'.format(autocorr_index))

    file_contents = str(file_contents[autocorr_index:autocorr_index + 50]).lower()
    file_contents = file_contents[len(autocorr_check):]
    autocorr_string = str(file_contents[:file_contents.find('/')])
    file_contents = file_contents[file_contents.find('/') + 1:]
    file_contents = str(file_contents[:file_contents.find('/')])
    ew_string = file_contents.replace(ew_check, '')

    del file_contents, response, ew_check, autocorr_check, autocorr_index

    return autocorr_string, ew_string
