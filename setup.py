import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="spiir-utils",
    version="0.0.2",
    author="Manoj Kovalam",
    author_email="manoj.kovalam@ligo.org",
    description="A test package for SPIIR",
    long_description=long_description,
    long_description_content_type="text/markdown",
    #insert URL
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: GNU License",
        "Operating System :: UNIX",
    ],
    extras_require = {
        'dev':  ['pytest>=3.7', 'ligo.skymap', 'jupyter',],
    },
)
