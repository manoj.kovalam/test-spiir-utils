import shutil

def get_skymap(client,obj_id):
    skymap = client.files(obj_id, 'bayestar.multiorder.fits')
    if skymap.status == 200:
        with open(obj_id+'.multiorder.fits', 'wb') as fh:
            shutil.copyfileobj(skymap, fh)
    else:
        return response


